<?php
/**
 * The main template file.
 *
 * @package toyota-child
 */

get_header(); ?>

<?php //if ( is_home() ) : ?>
	<!--<div class="entrant-banner lsx-full-width">	
	</div>-->
<?php //endif; ?>

<?php lsx_content_wrap_before(); ?>

<div id="primary" class="content-area <?php echo esc_attr( lsx_main_class() ); ?>">

	<?php lsx_content_before(); ?>

	<main id="main" class="site-main" role="main">

		<?php if ( is_home() ) : ?>
			<div class="entrants-title text-center">
				<h1>Entries</h1>
				<p class="big-text">Now’s your chance to earn the title and all the perks that come with it!</p>	
			</div>
			<?php get_search_form(); ?>
		<?php else : ?>
			<?php lsx_content_top(); ?>
		<?php endif; ?>

		<?php if ( have_posts() ) : ?>

			<div class="post-wrapper">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'partials/content', get_post_format() ); ?>

				<?php endwhile; ?>

			</div>

			<?php lsx_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'partials/content', 'none' ); ?>

		<?php endif; ?>

		<?php lsx_content_bottom(); ?>

	</main><!-- #main -->

	<?php lsx_content_after(); ?>

</div><!-- #primary -->

<?php lsx_content_wrap_after(); ?>

<?php get_sidebar( 'sidebar' ); ?>

<?php get_footer();
