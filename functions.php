<?php
/**
 * Toyota M24Travel functions
 *
 * @package 	toyota-child
 */

require get_stylesheet_directory() . '/classes/class-core.php';
//$theme_instance = \toyota_child\classes\Core::get_instance();


function nfs_voting_modal_body() {
	if ( is_single() ) {
		$voting_form_id = 2;
		if ( false !== $voting_form_id && '' !== $voting_form_id ) {
			?>
			<div class="modal fade" id="votingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
				 aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title"><?php esc_html_e( 'Vote', 'nfs-lsx-child-theme' ); ?></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<?php echo do_shortcode( '[gravityform id="' . $voting_form_id . '" title="false" description="false" ajax="true" ]' ); ?>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	}
}
add_action( 'wp_footer', 'nfs_voting_modal_body' );

/*** CUTOME THUMBNAILS SIZE ***/
add_action( 'after_setup_theme', 'toyota_theme_setup' );
function toyota_theme_setup() {
	add_image_size( 'custom-thumb', 280, 370, true );
}