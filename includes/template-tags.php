<?php
/**
 * Template tags for the theme
 *
 * @package    nff-lsx-child-theme
 * @subpackage template-tags
 */



/**
 * VOTING FORM MODAL
 */
function nff_voting_modal_body() {
	if ( is_single() ) {
		$voting_form_id = nff_get_option( 'nff_vote_form' );
		if ( false !== $voting_form_id && '' !== $voting_form_id ) {
			?>
			<div class="modal fade" id="votingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
				 aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title"><?php esc_html_e( 'Vote', 'nff-lsx-child-theme' ); ?></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<?php echo do_shortcode( '[gravityform id="' . $voting_form_id . '" title="false" description="false" ajax="true" ]' ); ?>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	}
}
add_action( 'wp_footer', 'nff_voting_modal_body' );