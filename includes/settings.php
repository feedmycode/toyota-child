<?php
/**
 * Created by PhpStorm.
 * User: krugazul
 * Date: 2018/09/20
 * Time: 2:33 PM
 */

/**
 * @param string $option
 *
 * @return string
 */
function nfs_get_option( $option = '', $tab = 'display' ) {
	$value = '';
	if ( '' !== $option ) {
		$options = get_option( '_lsx_settings', false );

		if ( false === $options ) {
			$options = get_option( '_lsx_lsx-settings', false );
		}

		if ( isset( $options['display'] ) && isset( $options['display'][ $option ] ) ) {
			$value = $options['display'][ $option ];
		}
	}
	return $value;
}

/**
 * gets the currenct gravity forms
 * @return array
 */
function nfs_get_gravity_forms() {
	global $wpdb;

	$results = \RGFormsModel::get_forms( null, 'title' );
	$forms   = false;

	if ( ! empty( $results ) ) {
		foreach ( $results as $form ) {
			$forms[ $form->id ] = $form->title;
		}
	}

	return $forms;
}

/**
 * Outputs the display tabs settings.
 */
function nfs_display_settings( $tab = 'general' ) {
	if ( 'placeholders' === $tab ) {
		?>
		<tr class="form-field">
			<th scope="row" colspan="2">
				<h3><?php esc_html_e( 'Voting', 'lsx-banners' ); ?></h3>
			</th>
		</tr>
		<tr class="form-field-wrap">
			<th scope="row">
				<label for="enquiry"><?php esc_html_e( 'Voting Form', 'tour-operator' ); ?></label>
			</th>
			<?php
				$forms = nfs_get_gravity_forms();
				$selected_form = nfs_get_option( 'nfs_vote_form' );
			?>
			<td>
				<select value="{{nfs_vote_form}}" name="nfs_vote_form">
					<?php
					if ( false !== $forms && '' !== $forms ) { ?>
						<option value="" {{#is nfs_vote_form value=""}}selected="selected"{{/is}}><?php esc_html_e( 'Select a form', 'nfs-lsx-child' ); ?></option>
						<?php
						foreach ( $forms as $form_id => $form_data ) { ?>
							<option value="<?php echo esc_attr( $form_id ); ?>" <?php if ( $selected_form == $form_id ) { echo esc_attr( 'selected="selected"' ); } ?>  ><?php echo esc_html( $form_data ); ?></option>
							<?php
						}
					} else { ?>
						<option value="" {{#is nfs_vote_form value=""}}selected="selected"{{/is}}><?php esc_html_e( 'You have no form available', 'nfs-lsx-child' ); ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<?php
	}
}
add_action( 'lsx_framework_display_tab_content', 'nfs_display_settings', 35, 1 );