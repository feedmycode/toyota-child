<?php
/**
 * Template used to display post content on single pages.
 *
 * @package toyota-child
 */
?>

<?php
	$images = get_attached_media( 'image' );

	if ( function_exists( 'get_field' ) ) {
		$city  = get_field( 'city_province', $post_id );
		$video = wp_oembed_get( get_field( 'youtube_link', $post_id ), array( 'width' => 920 ) );
	} else {
		$city = '';
		$video = '';
	}
?>

<?php lsx_entry_before(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-meta">
		<?php lsx_post_meta_single_top(); ?>
	</div><!-- .entry-meta -->

	<h1 class="wh-entry-title"><?php the_title(); ?><span class="pull-right vote-section"><?php echo do_shortcode( '[lsx_sharing_buttons buttons="facebook,twitter"]'); ?>
<!-- 	<?php
	$voting_form_id = 2;
	if ( false !== $voting_form_id && '' !== $voting_form_id ) { ?>
		<a href="#" class="btn vote-btn" data-toggle="modal" data-target="#votingModal"><?php esc_html_e( 'Vote for me!', 'nfs-lsx-child-theme' ); ?> <i class="far fa-thumbs-up"></i></a>
	<?php } ?> -->
	</span></h1>


	<?php if ( ! empty( $images )) {

		$image_ids = array();
		foreach ( $images as $image ) {
			$image_ids[] = $image->ID;
		}
		?>
		<div class="row photos">
			<h2>My Photo's</h2>
			<?php echo do_shortcode( '[gallery size="custom-thumb" link="media" ids="' . implode( ',', $image_ids ) . '"]' ); ?>
		</div>
	<?php } ?>

	<?php if ( ! empty( $video ) ) { ?>
		<div class="row video">
		<h2>My Video</h2>
		<div>
			<?php echo $video; ?>
		</div>
	</div>	
	<?php } ?>

	<div class="entry-content motivation">
		<h2>My Story</h2>
		<p class="city">From: <?php echo esc_html( $city ); ?></p>
		<?php
			the_content();

			wp_link_pages( array (
				'before'      => '<div class="lsx-postnav-wrapper"><div class="lsx-postnav">',
				'after'       => '</div></div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>

	</div><!-- .entry-content -->

	<?php lsx_entry_bottom(); ?>

</article><!-- #post-## -->

<?php lsx_entry_after(); ?>
